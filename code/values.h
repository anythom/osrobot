#ifndef VALUES
#define VALUES

float x = 0; // x-position in the field
float y = 0; // y-position in the field
int direction = 0;
int role = 0; // 0 = attacker, 1 = defender
int ambi_light = 0;
int start_direction = 0;
int origin_direction = 90;
bool have_ball = false;
bool end_search = false;
int max_way_back = 20;

#endif
