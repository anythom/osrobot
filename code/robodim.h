#ifndef ROBODIM
#define ROBODIM

const float WHEEL_DIAMETER = 5.5; //cm
const float WHEEL_TURN = 17.279; //cm
const float ANGLE_US_SENSOR = 37; // degree
const int ROBO_LENGTH = 30; //cm
const int ROBO_WIDTH = 26; //cm

#define RED_BALL_DIFF 12
#define BLUE_BALL_DIFF 6

#endif
