// The field is 1.2 meters large and 2 meters long, and protected on all sides with a fence high of 7 cm.

#ifndef PLAYGROUND
#define PLAYGROUND

#define WIDTH 120 //cm
#define LENGTH 200 //cm
#define DIRECTION_OPPOSITE_CAMP 0
#define MARGIN 5 //cm

#endif
